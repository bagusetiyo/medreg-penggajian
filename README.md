# Changelog MedReg Penggajian

* Added   : untuk penambahan fitur.
* Changed : untuk perubahan pada menu yang sudah ada atau optimasi.
* Removed : untuk menghapus fitur.
* Fixed   : untuk perbaikan bug atau error.

## [3.4.0.6] - 2018-03-07
### Menu Laporan Pemeriksaan Pasien
* **Fixed :**
* - Fix salah menampilkan data nama poli.

## [3.4.0.5] - 2017-12-29
### Menu Master Data Pasien
* **Changed :**
* - Ubah format cetak kartu A4, Portrait dan Landscape.
* - Ganti jenis barcode tipe code39 di cetak kartu pasien.
### Menu Master Data Kelurahan
* **Fixed :**
* - Fix tidak bisa input kecamatan ketika kota belum diisi.
* - Fix ketika hapus kecamatan / kota tombol kembali disable.
### Menu Backup Database
* **Changed :**
* - Ubah default format menjadi MedRegPenggajian Backup yyyy_MM_dd hh_mm_ss.mdb.
### System
* **Changed :**
* - Hapus list item data dokter mulai adqSenin - adqAllDay.
* - Ubah relative databasepath ketika load awal instalasi.